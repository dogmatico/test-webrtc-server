// Signaling
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var fs = require('fs');


// WebRTC
var webrtc = require('wrtc');
var RTCPeerConnection     = webrtc.RTCPeerConnection;
var RTCSessionDescription = webrtc.RTCSessionDescription;
var RTCIceCandidate       = webrtc.RTCIceCandidate;




io.on('connection', function (socket) {
  console.log('a user connected');
  socket.on('newOffer', offer => {
    var pc = new RTCPeerConnection();
    /*pc.onicecandidate = (e) => {
      console.log('Candidate');
      console.log(e);
      if(!e.candidate) return;
      pc.addIceCandidate(e.candidate);
    };*/

    socket.on('ICECandidate', pc.addIceCandidate);


    console.log('Received ');
    pc.setRemoteDescription(new RTCSessionDescription(offer), function() {
      console.log('createAnswer');
      pc.createAnswer(answer => {
        pc.setLocalDescription(new RTCSessionDescription(answer), () => {
          console.log('Set Local Description');
          socket.emit('newResponse', answer);
        }, err => {
          console.log(err);
        })
      }, err => {
        console.log(err);
      });
    }, err => {
      console.log('EEEE');
      console.log(err)
    });

    pc.ondatachannel = function(event) {
      const dataChannel = event.channel;
      dataChannel.onopen = function() {
        console.log("pc2: data channel open");
        dataChannel.onmessage = function(event) {
          var data = event.data;
          console.log("dc2: received '" + data + "'");
          dataChannel.send('pong');
        };
      };
    };
  });
});

/*fs.watchFile('./pgn4web-code/live/live.pgn', (curr, prev) => {
  console.log(`the current mtime is: ${curr.mtime}`);
  console.log(`the previous mtime was: ${prev.mtime}`);
  io.sockets.emit('updatedPGN');
});*/


http.listen(10003, function(){
  console.log('listening on *:10003');
});
